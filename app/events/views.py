from rest_framework import generics
from events.serializers import EventDetailsSerializer, EventsListSerializer
from events.models import Events
# Create your views here.

class EventCreateView(generics.CreateAPIView):
    serializer_class = EventDetailsSerializer


class EventsListView(generics.ListAPIView):
    serializer_class = EventsListSerializer
    queryset = Events.objects.all()