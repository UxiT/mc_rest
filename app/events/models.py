from django.db import models

# Create your models here.

"""
organization id int
title varchar 100
description text
category varchar 15
date date
time time
phone varchar 20
adrress varchar 100
geoposition varchar 45
price int
description_price int 
checked tinyint
created_at timestamp
apdated_at timestamp

"""


class Events (models.Model):
    title = models.CharField(max_length=100, verbose_name='Название мероприятия')
    description = models.TextField
    category = models.CharField(max_length=15,verbose_name='Категория')
    date = models.DateField
    time = models.TimeField
    phone = models.CharField(max_length=20,verbose_name='Номер телефона')
    address = models.CharField(max_length=100,verbose_name='Адрес')
    price = models.IntegerField(verbose_name='price')
    description_price = models.IntegerField(verbose_name='description_price')
    checked = models.SmallIntegerField(verbose_name='checked')
    created_at = models.DateTimeField()
    update_at = models.DateTimeField()



