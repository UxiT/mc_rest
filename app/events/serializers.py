from rest_framework import serializers
from events.models import Events


class EventDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Events
        fields = ('id', 'title', 'description ', 'date ')


class EventsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Events
        fields = '__all__'