MC DjangoRestAPI (version TEST)

* Для запуска требуеться кланировать репозиторий
* зайти в папку и с помощью команды python -m venv env создать вертуальное окружение
* перейти cd env/Scripts/
* активировать его activate
* с помощью команды скачать нужные пакеты pip install -r requirements.txt 

Для запуска проекта перейти в папку app и прописать команду ./manage.py runserver

## Для теста API
переодить на URL
 *http://127.0.0.1:8000/api/v1/event/create/ - для создания мероприятий
 *http://127.0.0.1:8000/api/v1/events/ - просмотор всех мероприятий